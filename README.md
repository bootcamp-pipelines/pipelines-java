# Pipelines Java

Pipelines is a plugin for Bitbucket. The purpose of Pipelines is to enable developers to use [CI/CD](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd). This will enable developers to have tests executed typically on any git push to Bitbucket. This gives developers a sense of confidence, ensuring that their commit(s) do not introduce new problems to the codebase. This is, however, not a guarantee that there will not be any problems.

This is an example repo showing [Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html) in a [Java] environment. 

* Pipeline Example Repos:
  * [Python](https://bitbucket.org/bootcamp-pipelines/pipelines-python/src/master/)

  * [Laravel](https://bitbucket.org/bootcamp-pipelines/pipelines-laravel/src/master/)

  * [Node](https://bitbucket.org/bootcamp-pipelines/pipelines-node/src/master/)

  * [PHP](https://bitbucket.org/bootcamp-pipelines/pipelines-php/src/master/)

  * [Ruby](https://bitbucket.org/bootcamp-pipelines/pipelines-ruby/src/master/)

[Java Bitbucket Pipelines Reference Page](https://confluence.atlassian.com/bitbucket/java-with-bitbucket-pipelines-872013773.html)


## Setup

NOTE: Make sure you have Maven installed.

1. Fork this repository by clicking the plus sign and then "Fork this repository"

![Fork Repository Tutorial](https://media.giphy.com/media/35MvfIa8XbwnDRD0g5/giphy.gif)

2. On the left navigation bar, click on "Pipelines" Scroll down to view the bitbucket-pipelines.yml file and click "Enable"

![Enable Pipelines Tutorial](https://media.giphy.com/media/3JTpamHn2MX0RrO7SJ/giphy.gif)

3. Watch your build run

![Build Progress](https://media.giphy.com/media/82l6PwEc47oTtkfh7i/giphy.gif)

NOTE: Bitbucket Pipelines includes fifty free minutes per repo, at the time of writing. The remaining minutes can be checked by clicking on the "Usage" button on the top right under in the "Pipelines" tab.


## Basic Commands

File: `bitbucket-pipelines.yml`


```
image: maven:3.3.9

pipelines:
  default:
    - step:
        caches:
          - maven
        script: # Modify the commands below to build your repository.
          - mvn -B verify # -B batch mode makes Maven less verbose
```

For in-depth configuration information, visit the Bitbucket Pipelines [YAML Configuration Page](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html). A list of the more useful keywords at listed below.

`step`: Each step loads a new Docker container that includes a clone of the current repository. The contents in each script keyword is then executed sequentially.

`script`: A list of commands that are executed sequentially.

`caches`: Re-downloading dependencies from the internet for each step of a build can take a lot of time. The cache can be specified to store content so it can be reused upon each build.


## Advanced Commands

Connecting to a database is also quite simple, by adding a few lines to `bitbucket-pipelines.yml`. This will specify and define the database.

The resulting file may look something like this:


```
image: maven:3.3.9

pipelines:
  default:
    - step:
        caches:
          - maven
        script: # Modify the commands below to build your repository.
          - mvn -B verify # -B batch mode makes Maven less verbose
          
        services: 
          - mongo 

definitions: 
  services: 
    mongo: 
      image: mongo
```
![Checking Your Database Works](https://media.giphy.com/media/1X630FnLplHgNkN2qo/giphy.gif)

For different database examples, visit the Bitbucket Pipelines [Database Page](https://confluence.atlassian.com/bitbucket/test-with-databases-in-bitbucket-pipelines-856697462.html).
